﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace LandrySite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // Home Page Route
            routes.MapRoute(
                name: "Home",
                url: "",
                defaults: new { controller = "Home", action = "Index" });

            // Course Page Route
            routes.MapRoute(
                name: "Course",
                url: "course/{course}", defaults: new { controller = "Home", action = "Course", course = "" });

            // Third wheel route
            // Loads the page that is grabbed via AJAX
            routes.MapRoute(
                name: "Lesson",
                url: "loadlesson/{page}/{course}",
                defaults: new { controller = "Home", action = "loadlesson"});

            routes.MapRoute(
                name: "Template",
                url: "template",
                defaults: new { controller = "Home", action = "template" });
        }
    }
}
