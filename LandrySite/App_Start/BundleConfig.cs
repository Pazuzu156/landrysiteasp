﻿using System.Web;
using System.Web.Optimization;

namespace LandrySite
{
    /// <summary>
    /// Bundling class for bundling together CSS and JavaScript files
    /// </summary>
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/assets/css/theme.css",
                      "~/assets/css/animate.css",
                      "~/assets/css/site.css"));

            bundles.Add(new ScriptBundle("~/Content/js").Include(
                      "~/assets/js/jquery.js",
                      "~/assets/js/bootstrap.js",
                      "~/assets/js/notify.js",
                      "~/assets/js/cookie.js",
                      "~/assets/js/site.js"));
        }
    }
}
