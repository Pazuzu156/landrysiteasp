﻿using LandrySite.App_Start;
using LandrySite.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace LandrySite.Controllers
{
    /// <summary>
    /// The only controller here. Just makes sure pages are loaded
    /// </summary>
    public class HomeController : Controller
    {
        // Index / Home Page. Nothing special here..
        [CacheFilter(Duration = 60)]
        public ActionResult Index()
        {
            return View();
        }
        
        /// <summary>
        /// Loads the actual course. Everything should be handled here
        /// Any data should be passed to the view, and the view shouldn't
        /// call any data the controller does not pass it.
        /// </summary>
        /// <param name="course"></param>
        /// <returns></returns>
        [CacheFilter(Duration = 60)]
        public ActionResult Course(string course)
        {
            // If no course is given, throw them home
            if (course.Equals(""))
                return RedirectToAction("Index", "Home");
            else
            {
                // list of strings to ignore in the UCWords class call
                List<string> il = new List<string>()
                {
                    "in", "the", "of", "for", "if", "a", "to"
                };
                string oldTitle = course.Replace('-', ' ');
                StringBuilder builder = new StringBuilder();

                // Handles page title and class title search/replace/capitalization
                // from URL
                foreach(string word in oldTitle.Split(' '))
                {
                    if (!il.Contains(word))
                    {
                        if (word.Length >= 4 || word.ToLower().Equals("web"))
                            builder.Append(UCWords.Exec(word) + " ");
                        else
                            builder.Append(word.ToUpper() + " ");
                    }
                    else
                        builder.Append(word + " ");
                }

                // Add the title obtained above passing it the ViewBag
                ViewBag.Title = builder.ToString().Trim();
                return View();
            }
        }

        /// <summary>
        /// Handles loading the actual lesson. This function returns a view with NO corrolation
        /// to any of the other views/layout. This is the AJAX call's job to style it by passing it
        /// into a view already styled. Called from the course layout through JavaScript
        /// </summary>
        /// <param name="page"></param>
        /// <param name="course"></param>
        /// <returns></returns>
        [CacheFilter(Duration = 60)]
        public ActionResult loadlesson(string page, string course)
        {
            // Extract the lesson number from the URL
            var exp = page.Split('-');
            string month = exp[1].ToLower();
            string lesson = "lesson_" + exp[3];
            string courseName = Server.UrlEncode(course);

            string pageName = lesson + ".templ";

            // Pass the absolute path of the file into the ViewBag
            ViewBag.LessonPath = Server.MapPath("~/Courses/" + course + "/lessons/" + month + "/" + pageName);

            return View();
        }

        [CacheFilter(Duration = 60)]
        public ActionResult template()
        {
            ViewBag.Title = "Template";
            return View();
        }
    }
}