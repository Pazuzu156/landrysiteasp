﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Web.Mvc;

namespace LandrySite.Classes
{
    public class TemplateLoader
    {
        private string m_content;

        /// <summary>
        /// TemplateLoader class loads in the template file
        /// specified when instantiated
        /// <param name="templatePath"></param>
        /// </summary>
        public TemplateLoader(string templatePath)
        {
            // Try/Catch
            try
            {
                // Open file, read content and put it into the string, close all afterwords
                // to clean up memory
                FileStream file = File.Open(templatePath, FileMode.Open, FileAccess.Read);
                StreamReader reader = new StreamReader(file);
                m_content = reader.ReadToEnd();
                reader.Close();
                file.Close();
            }
            // File not found, throw this exception
            catch(FileNotFoundException ex)
            {
                throw new FileNotFoundException("The template: " + templatePath + ".templ could not be found!\r\n\r\nException: " + ex);
            }
            // File cannot be loaded, throw this exception
            catch(FileLoadException ex)
            {
                throw new FileLoadException("The template could not be loaded!\r\n\r\nException: " + ex);
            }
        }

        // m_Content is private
        // This is a getter, to obtain it.
        public string getTemplate()
        {
            return this.m_content;
        }
    }
}