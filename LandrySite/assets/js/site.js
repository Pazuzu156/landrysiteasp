﻿/*
    Mr. Landry's Website | site.min.js
    This JavaScript code is (c) 2015 Kaleb Klein
*/
$(function () {
    //$("#popup").modal();

    $("#icon-refresh").animate({ borderSpacing: 360 }, {
        step: function (now, fx) {
            $(this).css('-webkit-transform', 'rotate(' + now + 'deg)');
            $(this).css('-moz-transform', 'rotate(' + now + 'deg)');
            $(this).css('transform', 'rotate(' + now + 'deg)');
        },
        duration: 'slow'
    });

    $('[data-parent="#accordian"]').click(function () {
        var expanded = $(this).attr('aria-expanded');
        var controls = $(this).attr('aria-controls');

        $(this).attr('aria-expanded', (expanded === undefined) ? '' : expanded);
        $.cookie('month', controls, { expires: 5 });
    });

    var pl = (window.location + "").split('/').slice(-1);

    var pageCookie = $.cookie('page');
    var monthCookie = $.cookie('month');
    var courseCookie = $.cookie('course');
    if (pageCookie !== undefined) {
        var l = window.location.pathname.split('/').slice(-1);
        if (courseCookie === l[0])
            loadPage(pageCookie, pl, true);
    }
    if (monthCookie !== undefined) {
        var l = window.location.pathname.split('/').slice(-1);
        if (courseCookie === l[0])
            setOpenMonth(monthCookie);
    }

    $('[data-toggle="tooltip"]').tooltip();

    $("#refresh").click(function () {
        window.location.reload();
    });

    $("#clear").click(function () {
        $.removeCookie('page');
        $.removeCookie('month');
        $.removeCookie('course');

        if ($.cookie('noshow') === undefined)
            $("#popup").modal();
        else {
            var n = $.notify('<span class="notify-text">The course history has been cleared!</span>', {
                delay: 0,
                placement: {
                    from: "top",
                    align: "center"
                },
                type: 'success',
                animate: {
                    enter: 'animated bounceInDown',
                    exit: 'animated bounceOutUp'
                }
            });

            setTimeout(function () {
                n.close();
            }, 3500);
        }
    });

    $("#alert-modal-close").click(function () {
        var noshow = $("#no-show");

        if (noshow.is(":checked"))
            $.cookie('noshow', 'true');
    });

    $("a").click(function () {
        if (this.hash) {
            var hash = this.hash.substr(1);

            var day = $(this).parent().html().split(':')[0];
            var monthDD = $(this).parent().parent().parent().attr('id');
            var month = $("a[href=#" + monthDD + "]").html().trim();

            var page = day + '-' + month + '-' + hash;

            $("#lesson").toggle(function () {
                $(this).hide('slow');
            },
            function () {
                loadPage(page, pl, false);
                $(this).show('slow');
            });

            return false;
        }
    });
});

function setOpenMonth(month) {
    if (month !== 'monthOne') {
        $('[aria-controls="monthOne"]').attr('aria-expanded', 'false');
        $("#monthOne").removeClass('in');
        $('[aria-controls="' + month + '"]').attr('aria-expanded', 'true');
        $("#" + month).addClass('in');
    }
}

function loadPage(page, course, showNotify) {
    var r = "";
    $.get('/loadlesson/' + page + '/' + course, function (data) {
        $("#lesson").html(data);
        r = data;
        $.cookie('page', page, { expires: 5 });
        $.cookie('course', course, { expires: 5 });

        var n = $.notify('<span class="notify-text">The lesson page has been loaded!</span>', {
            delay: 0,
            placement: {
                from: "top",
                align: "center"
            },
            type: 'success',
            animate: {
                enter: 'animated bounceInDown',
                exit: 'animated bounceOutUp'
            }
        });

        setTimeout(function () {
            n.close();
        }, 3500);
    }, 'html');
}
