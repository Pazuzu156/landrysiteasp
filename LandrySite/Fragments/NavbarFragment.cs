﻿using LandrySite.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace LandrySite.Fragments
{
    public class NavbarFragment : Fragment
    {
        /// <summary>
        /// This fragment is responsible for loading/parsing lesson navigation templates
        /// </summary>
        /// <param name="templatePath"></param>
        public NavbarFragment(string templatePath) : base(templatePath) { }

        /// <summary>
        /// This method takes care of parsing and returning
        /// the lesson nav template in full HTML
        /// </summary>
        /// <returns>System.Web.IHtmlString</returns>
        public IHtmlString getFragment()
        {
            var patterns = getPatterns();
            var replacements = getReplacements();

            for (int i = 0; i < patterns.Count; i++)
            {
                rgx = new Regex(patterns[i]);
                m_Template = rgx.Replace(m_Template, replacements[i]);
            }

            m_Template = Regex.Replace(m_Template, @"monthtitle:\s(.*)", TitleCallback);
            m_Template = Regex.Replace(m_Template, @"date:\s(.*)", DateCallback);

            // Comments requires special treatment....
            m_Template = Regex.Replace(m_Template, @"\/\*(.+)\*\/\r?", "", RegexOptions.Singleline);

            return new HtmlString("<div class=\"col-sm-3\"><div class=\"panel-group\" id=\"accordian\" role=\"tablist\" aria-multiselectable=\"true\">"
                + m_Template
                + "</div></div>");
        }

        /// <summary>
        /// This is a callback method for Regex.Replace.
        /// This method replaces month titles with HTML accordian headers
        /// </summary>
        /// <param name="match"></param>
        /// <returns>string</returns>
        private string TitleCallback(Match match)
        {
            string title = match.Groups[1].Value;
            string month = title.ToLower().Trim();

            string replace = "<div class=\"panel-heading\" role=\"tab\">"
                            + "<h4 class=\"panel-title\">"
                            + "<a data-toggle=\"collapse\" data-parent=\"#accordian\" href=\"#" + month + "\" aria-expanded=\"true\" aria-controls=\"" + month + "\">"
                            + title
                            + "</a></h4></div>"
                            + "<div id=\"" + month + "\" class=\"panel-collapse collapse role=\"tabpanel\" aria-labeledby=\"headingOne\">";

            return replace;
        }

        /// <summary>
        /// This method takes care of parsing the dates tempalate tag
        /// </summary>
        /// <param name="match"></param>
        /// <returns></returns>
        private string DateCallback(Match match)
        {
            string[] s = match.Groups[1].Value.Split(':');

            string name = s[1].ToLower().Trim();

            string replace = "<span>" + s[0] + ": "
                + "<a href=\"#" + name.Replace(' ', '-') + "\" class=\"lesson-link\">" + s[1] + "</a>"
                + "</span><br>";

            return replace;
        }
    }
}