﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace LandrySite.Fragments
{
    public class MenuFragment : Fragment
    {
        /// <summary>
        /// This fragment handles the main navigation menu for the site.
        /// For a fragment with the 2nd most important part, it's kinda small
        /// </summary>
        /// <param name="templatePath"></param>
        public MenuFragment(string templatePath) : base(templatePath) { }

        /// <summary>
        /// This method takes care of parsing and returning
        /// the site menu in full HTML
        /// </summary>
        /// <returns>System.Web.IHtmlString</returns>
        public IHtmlString getFragment()
        {
            var patterns = getPatterns();
            var replacements = getReplacements();

            for (int i = 0; i < patterns.Count; i++)
            {
                rgx = new Regex(patterns[i]);
                m_Template = rgx.Replace(m_Template, replacements[i]);
            }

            // Comments requires special treatment....
            m_Template = Regex.Replace(m_Template, @"\/\*(.+)\*\/\r?", "", RegexOptions.Singleline);

            return new HtmlString(m_Template);
        }
    }
}