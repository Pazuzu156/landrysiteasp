﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace LandrySite.Fragments
{
    public class LessonFragment : Fragment
    {
        /// <summary>
        /// This fragment is responsible for loading/parsing lesson templates
        /// </summary>
        /// <param name="templatePath"></param>
        public LessonFragment(string templatePath) : base(templatePath) { }

        /// <summary>
        /// This method just get's the home page for each course.
        /// This is just an HTML file, so no parsing is needed here.
        /// </summary>
        /// <returns>System.Web.IHtmlString</returns>
        public IHtmlString getHomePage()
        {
            StringBuilder builder = new StringBuilder();

            if(this.m_Template != null)
            {
                foreach (var line in m_Template)
                {
                    builder.Append(line);
                }
            }
            else
            {
                return null;
            }

            return new HtmlString(builder.ToString());
        }

        /// <summary>
        /// This method takes care of parsing and returning
        /// the lesson template in full HTML
        /// </summary>
        /// <returns>System.Web.IHtmlString</returns>
        public IHtmlString getFragment()
        {
            var patterns = getPatterns();
            var replacements = getReplacements();

            for (int i = 0; i < patterns.Count; i++)
            {
                rgx = new Regex(patterns[i]);
                m_Template = rgx.Replace(m_Template, replacements[i]);
            }

            m_Template = Regex.Replace(m_Template, @"(#{1,})\s(.*)\s(?:#{1,})", AnchorCallback);
            m_Template = Regex.Replace(m_Template, ":tab([0-9]{1,3})?:", TabCallback);

            // Comments require special treatment....
            m_Template = Regex.Replace(m_Template, @"\/\*(.+)\*\/\r?", "", RegexOptions.Singleline);

            return new HtmlString(m_Template);
        }

        /// <summary>
        /// This is a callback method for Regex.Replace.
        /// This method replaces anchor template tags with HTML anchor tags
        /// </summary>
        /// <param name="match"></param>
        /// <returns>string</returns>
        private string AnchorCallback(Match match)
        {
            int count = match.Groups[1].Value.Length;
            return "<h" + count + ">" + match.Groups[2].Value + "</h" + count + ">";
        }

        /// <summary>
        /// This callback is for parsing the tab tempalate tag
        /// with HTML equivilant tabbing.
        /// </summary>
        /// <param name="match"></param>
        /// <returns>string</returns>
        private string TabCallback(Match match)
        {
            int num = (match.Groups[1].Value.Equals("")) ? 1 : int.Parse(match.Groups[1].Value);
            string tab = "";
            for(int i = 0; i < num; i++)
            {
                tab += "&nbsp;&nbsp;&nbsp;&nbsp;";
            }
            return tab;
        }
    }
}