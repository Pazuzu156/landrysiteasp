﻿using LandrySite.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace LandrySite.Fragments
{
    /// <summary>
    /// The templates are based on a fragmentation system. This class is the parent class.
    /// All fragments should extend this class as it contains all required prerequisites
    /// for loading/parsing templating files.
    /// </summary>
    public class Fragment
    {
        protected string m_Template;
        protected Regex rgx;

        private List<string> patterns;
        private List<string> replacements;

        public Fragment(string templatePath)
        {
            load(templatePath);
            registerLists();
        }

        /// <summary>
        /// Registers the lists that the child classes will use for parsing templating files
        /// </summary>
        private void registerLists()
        {
            patterns = new List<string>()
            {
                "--(.*)--",
                "__(.*)__",
                @"//\s(.*)",
                "ulist:",
                @"olist(\-([aAiI1]))?:",
                ":endulist",
                ":endolist",
                @"item:\s(.*)",
                "anchor:\\s(http:\\/\\/|https:\\/\\/)([a-zA-Z0-9._\\-\\?\\/\\=\\%\\~]+)(\\s\\|\\s(.*)\\s:anchor)",
                "anchor:\\s(http:\\/\\/|https:\\/\\/)([a-zA-Z0-9._\\-\\?\\/\\=\\%\\~]+)",
                "table:",
                "trow:",
                "tcell:",
                ":endtcell",
                ":endtrow",
                ":endtable",
                //@"\/\*(.+)\*\/\r?",
                
                // Main menu stuff
                "navlink:",
                @"url:\s(.*)",
                @"url-extern:\s(.*)",
                @"linkname:\s(.*)",
                ":endnavlink",
                "menu:",
                @"menuname:\s(.*)",
                @"menuheader:\s(.*)",
                ":divider:",
                ":endmenu",

                // Navbar menu stuff
                "month:",
                ":endmonth",
                "dates:",
                ":enddates"
            };

            replacements = new List<string>()
            {
                "<b>$1</b>",
                "<u>$1</u>",
                "",
                "<ul>",
                "<ol type=\"$2\">",
                "</ul>",
                "</ol>",
                "<li>$1</li>",
                "<a href=\"$1$2\" target=\"_blank\">$4</a>",
                "<a href=\"$1$2\" target=\"_blank\">$1$2</a>",
                "<table class=\"table\">",
                "<tr>",
                "<td>",
                "</td>",
                "</tr>",
                "</table>",
                //"",

                // Main menu stuff
                "<li>",
                "<a href=\"$1\">",
                "<a href=\"$1\" target=\"_blank\">",
                "$1</a>",
                "</li>",
                "<li class=\"dropdown\">",
                "<a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">$1</a><ul class=\"dropdown-menu\">",
                "<li role=\"presentation\" class=\"dropdown-header\">$1</li>",
                "<li role=\"presentation\" class=\"divider\"></li>",
                "</ul>",

                // Navbar menu stuff
                "<div class=\"panel panel-default\">",
                "</div>",
                "<div class=\"panel-body\">",
                "</div></div>"
            };
        }

        /// <summary>
        /// Loads the file using the TemplateLoader and sets the protected m_Template with the content
        /// </summary>
        /// <param name="templatePath"></param>
        public void load(string templatePath)
        {
            try
            {
                var loader = new TemplateLoader(templatePath);

                this.m_Template = loader.getTemplate();
            }
            catch (Exception)
            {
                this.m_Template = null; // Return null for error checking!
            }
        }

        /// <summary>
        /// Getter for the Patterns List
        /// </summary>
        /// <returns>List&lt;String&gt;</returns>
        public List<string> getPatterns()
        {
            return patterns;
        }

        /// <summary>
        /// Getter for the Replacements List
        /// </summary>
        /// <returns>List&lt;String&gt;</returns>
        public List<string> getReplacements()
        {
            return replacements;
        }
    }
}