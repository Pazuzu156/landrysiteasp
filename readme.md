# Mr. Landry's Website
This is the ASP version of the website. All code is fully commented, and setup instructions are included in this readme.

## Contents
[Prerequisites][1]  
[Installation][2]  
[Execution][3]

## Prerequisites
IIS  
[.NET 4.5][4]
[HtmlOptimizerMvc4][7]

## Installation
To install, you need to install and setup IIS first. To set this up, follow these directions.  

1. Navigate to Control Panel > Programs and Features > Uninstall Program > Add/Remove Windows Features  
2. Click the checkbox on "Internet Information Systems" where the checkbox is a box, not a check mark.  
3. Allow IIS through your firewall - Control Panel > System and Security > Windows Firewall > Allow app through firewall
4. Check all the boxes corresponding to World Wide Web Services (HTTP) service
5. Create a new site in IIS Control Panel. Don't use the default site pool, make a new one. Allow IIS to do so for you
6. Copy all contents from the [download][5] into the directory you assigned the new site


## Execution
Navigate to the localhost to view the site: ``http://127.0.0.1 -OR- http://localhost``

To check the site remotely, be sure to forward port 80 through your router. Refer to your router's documentation on forwarding ports through the router. To access the site remotely, you need your public IP address. You can get this at [my port forwarding checker][6] as well as testing whether or not you correctly forwarded your port and opened a tunnel through your firewall.

The URL you will use if you do not attach a domain name to your server will be ``http://YOUR_PUBLIC_IP``

[//]: # (All links for this document)
[1]: #prerequisites
[2]: #installation
[3]: #execution
[4]: https://www.microsoft.com/en-US/Download/details.aspx?id=30653
[5]: https://bitbucket.org/Pazuzu156/landrysiteasp/downloads
[6]: http://port.kalebklein.com
[7]: https://github.com/Chebur9tina/HtmlOptimizerMvc4
